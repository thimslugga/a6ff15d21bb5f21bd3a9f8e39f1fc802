zz-sap_customer_aliases.sh

alias hgrep='history | grep $1'

alias tm='tail -100f /var/log/messages | grep -v systemd'
alias tms='tail -1000f /var/log/messages | grep -s -E "Setting master-rsc_SAPHana_$SAPSYSTEMNAME_HDB${TINSTANCE}|sr_register|WAITING4LPA|EXCLUDE as possible takeover node|SAPHanaSR|failed|${HOSTNAME}|PROMOTED|DEMOTED|UNDEFINED|master_walk|SWAIT|WaitforStopped|FAILED"'
alias tmsl='tail -1000f /var/log/messages | grep -s -E "Setting master-rsc_SAPHana_$SAPSYSTEMNAME_HDB${TINSTANCE}|sr_register|WAITING4LPA|PROMOTED|DEMOTED|UNDEFINED|master_walk|SWAIT|WaitforStopped|FAILED|LPT"'

alias sapstart='sapcontrol -nr ${TINSTANCE} -function StartSystem HDB; hdbi'
alias sapstop='sapcontrol -nr ${TINSTANCE} -function StopSystem HDB; hdbi'

alias sgsi='watch sapcontrol -nr ${TINSTANCE} -function GetSystemInstanceList'
alias spl='watch sapcontrol -nr ${TINSTANCE} -function GetProcessList'
alias splh='watch "sapcontrol -nr ${TINSTANCE} -function GetProcessList | grep hdbdaemon"'

alias srm='watch "hdbnsutil -sr_state --sapcontrol=1 |grep site.*Mode"'
alias srs="watch -n 5 'python /usr/sap/$SAPSYSTEMNAME/HDB${TINSTANCE}/exe/python_support/systemReplicationStatus.py; echo Status \$?'"
alias srstate='watch -n 10 hdbnsutil -sr_state'

alias hri='hdbcons -e hdbindexserver "replication info"'
alias hris='hdbcons -e hdbindexserver "replication info" | grep -E "SiteID|ReplicationStatus_"'

alias hdb='watch -n 5 "sapcontrol -nr ${TINSTANCE} -function GetProcessList | grep -s -E hdbdaemon\|hdbnameserver\|hdbindexserver"'
alias hdbi='watch -n 5 "sapcontrol -nr ${TINSTANCE} -function GetProcessList | grep -s -E hdbdaemon\|hdbnameserver\|hdbindexserver; sapcontrol -nr ${TINSTANCE} -function GetSystemInstanceList"'

alias vglo="vim /usr/sap/$SAPSYSTEMNAME/SYS/global/hdb/custom/config/global.ini"
alias vgloh="vim /hana/shared/${SAPSYSTEMNAME}/HDB${TINSTANCE}/${HOSTNAME}/global.ini"

alias gtr='watch -n 10 /usr/sap/$SAPSYSTEMNAME/HDB${TINSTANCE}/exe/Python/bin/python /usr/sap/$SAPSYSTEMNAME/HDB${TINSTANCE}/exe/python_support/getTakeoverRecommendation.py --sapcontrol=1'
alias lhc='/usr/sap/$SAPSYSTEMNAME/HDB${TINSTANCE}/exe/Python/bin/python /usr/sap/$SAPSYSTEMNAME/HDB${TINSTANCE}/exe/python_support/landscapeHostConfiguration.py; echo $?'