# Corosync Cheatsheet

Messaging layer in a Pacemaker High-Availability Cluster.

## Installation

RHEL:

```sh
dnf install -y corosync
```

SLES:

```sh
zypper install -y corosync
```

## Setup

```sh
cat /etc/hosts
```

```sh
cat /etc/resolv.conf
```

```sh
cat /etc/ntp.conf
```

corosync-keygen creates this key and writes it to /etc/corosync/authkey or to file specified by -k option.

```sh
corosync-keygen -l -k /etc/corosync/authkey
```

Note: corosync-keygen will ask for user input to assist in generating entropy unless the -l option is used. This option uses a less secure random data source that will not require user input to help generate entropy. This may be useful when this utility is used from a script or hardware random number generator is not available.

OR

```sh
dd if=/dev/random of=/etc/corosync/authkey bs=4 count=2048
```

Output:

```
/etc/corosync/authkey
```

```sh
chown root: /etc/corosync/authkey
chmod 0400 /etc/corosync/authkey
```

Copy /etc/corosync/authkey to all nodes. Copy the key to some security transportable storage or use ssh to transmit the key from node to node. This private key must be copied to every processor in the cluster. Corosync requires that each node possesses an identical cluster authorization key. If the private key isn't the same for every node, those nodes with non-matching private keys will not be able to join the same configuration.

```
scp /etc/corosync/authkey node02.x.x:/etc/corosync/
```

```sh
scp /etc/corosync/authkey node01.x.x:/etc/corosync/
```

Now all servers should have an identical authorization key in the /etc/corosync/authkey file. 


Next, edit /etc/corosync/corosync.conf and under the totem { } directive, set:

```
secauth: on
crypto_cipher: aes256
crypto_hash: sha256
```

For example:

```
totem {
    version: 2
    
    cluster_name: hacluster
    
    secauth: on
    crypto_cipher: aes256
    crypto_hash: sha256
    
    transport: udpu
}
```

Enabling secauth specifies that all data should be authenticated using HMAC authentication method specified in crypto_hash and encrypted with the encryption algorithm specified in crypto_cipher to protect data from eavesdropping.

The crypto_cipher specifies which cipher should be used to encrypt all messages. Valid values are none (no encryption), aes256, aes192, aes128 and 3des. Enabling crypto_cipher, requires also enabling of crypto_hash. The default is aes256.

The crypto_hash specifies which HMAC authentication should be used to authenticate all messages. Valid values are none (no authentication), md5, sha1, sha256, sha384 and sha512. The default is sha1.

Enabling secauth will increase header size of every message sent by totem which reduces total throughput. Encryption and authentication consume more CPU cycles when enabled.

Verify:

```sh
corosync-cmapctl | grep 'secauth\|crypto'  
```

## Corosync Configuration

View corosync configuration file:

```sh
cat /etc/corosync/corosync.conf
```

View diff of the configuration:

```sh
crm corosync diff
```

Transfer the file to other node:

```sh
scp /etc/corosync/corosync.conf root@node02:/etc/corosync/
```

### Totem Token

The token timeout definition from totem {} directive of corosync configuration file /etc/corosync/corosync.conf is only used as a base for runtime token timeout calculation.

```sh
corosync-cmapctl | grep totem.token
```

```sh
corosync-cmapctl totem.token
```

For token timeout used by totem at runtime, it's possible to read cmap value of runtime.config.totem.token key.

```sh
corosync-cmapctl runtime.config.totem.token
```

totem.token and totem.token_coefficient are used in calculating the real runtime.config.totem.token timeout using following formula:

```
runtime.config.token = totem.token + (number_of_nodes - 2) * totem.token_coefficient
```

This allows cluster to scale without manually changing token timeout every time new node is added. This value can be set to 0 resulting in effective removal of this feature. The default is 650 milliseconds.

## Corosync Service Commands

Check the status and for quorum:

```shell
corosync-cfgtool -s
corosync-quorumtool -il
```

View status of corosync via crmsh:

```sh
crm corosync status
```

View the status using pcs:

```sh
pcs status corosync
```

Corosync service status via systemd:

```sh
systemctl status corosync
```

Restart corosync service via systemd:

```sh
systemctl restart corosync
```

## Corosync CLI Tools

View cluster members:

```
corosync-cmapctl | grep members
```