# zz-cluster_aliases.sh

# As root add to your ~/.bashrc or ~/.bash_aliases

alias hgrep='history | grep $1'
alias tma='tmux attach -t $(tmux ls | grep -v atta | head -1 | cut -d " " -f 1)'
alias tm='tail -100f /var/log/messages | grep -v systemd'

## Pacemaker Cluster

#alias node01='ssh node01'
#alias node02='ssh node02'

# Provides an overview
alias cluster-overview='crm_mon -1Arf'
alias crmm='watch -n 1 crm_mon -1Arf'
alias cluster-config='pcs cluster config show'
alias cluster-properties='pcs property config'
# Cluster status
alias cluster-status='pcs status --full'
# Qurorum status
alias cluster-quorum='pcs quorum status'
alias cluster-fence-history='pcs stonith history'
# List node attributes
alias cluster-node-attrs='pcs node attribute'
# Lists all constraint ids which should be removed
alias cluster-constraints='pcs constraint --full'
# Lists all resources and shows if they are running
alias cluster-resources='pcs resource'

#alias cdhb='cd /usr/lib/ocf/resource.d/heartbeat'

# This will start the cluster on all nodes
alias cluster-start='pcs cluster start --all'
# This will stop the cluster on all nodes
alias cluster-stop='pcs cluster stop --all'

#alias killnode="echo 'b' > /proc/sysrq-trigger"

## SAP HANA

export ListInstances=$(/usr/sap/hostctrl/exe/saphostctrl -function ListInstances | head -1)
export Instance=$(echo "${ListInstances}" | cut -d " " -f 7)
export sid=$(echo "${ListInstances}" | cut -d " " -f 5 | tr [A-Z] [a-z])
export SID=$(echo ${sid} | tr [a-z] [A-Z])

#alias sap-python='/usr/sap/${SID}/HDB${Instance}/exe/Python/bin/python'
alias shr='watch -n 5 "SAPHanaSR-monitor --sid=${SID}"'

alias tms='tail -1000f /var/log/messages | grep -s -E "Setting master-rsc_SAPHana_${SID}_HDB${Instance}|sr_register|WAITING4
LPA|EXCLUDE as possible takeover node|SAPHanaSR|failed|${HOSTNAME}|PROMOTED|DEMOTED|UNDEFINED|master_walk|SWAIT|WaitforStop
ped|FAILED"'

alias tmss='tail -1000f /var/log/messages | grep -v systemd | grep -s -E "secondary with sync status|Setting master-rsc_SAPHa
na_${SID}_HDB${Instance}|sr_register|WAITING4LPA|EXCLUDE as possible takeover node|SAPHanaSR|failed|${HOSTNAME}|PROMOTED|DE
MOTED|UNDEFINED|master_walk|SWAIT|WaitforStopped|FAILED"'

alias tmsl='tail -1000f /var/log/messages | grep -s -E "Setting master-rsc_SAPHana_${SID}_HDB${Instance}|sr_register|WAITING
4LPA|PROMOTED|DEMOTED|UNDEFINED|master_walk|SWAIT|WaitforStopped|FAILED|LPT|SOK|SFAIL|SAPHanaSR-mon"'

alias tmm='tail -1000f /var/log/messages | grep -s -E "Setting master-rsc_SAPHana_${SID}_HDB${Instance}|sr_register|WAITING4
LPA|PROMOTED|DEMOTED|UNDEFINED|master_walk|SWAIT|WaitforStopped|FAILED|LPT|SOK|SFAIL|SAPHanaSR-mon" | grep -v systemd'

alias sapstart='su - ${sid}adm  -c sapstart'
alias sapstop='su - ${sid}adm  -c sapstop'
alias srstate='su - ${sid}adm  -c srstate'
alias cglo='su - ${sid}adm -c cglo'
alias gtr='su - ${sid}adm  -c gtr'
alias hdb='su - ${sid}adm  -c hdb'
alias hdbi='su - ${sid}adm  -c hdbi'
alias hri='su - ${sid}adm  -c hri'
alias hris='su - ${sid}adm  -c hris'
alias lhc='su - ${sid}adm  -c lhc'
alias sgsi='su - ${sid}adm  -c sgsi'
alias srm='su - ${sid}adm  -c srm'
alias srs='su - ${sid}adm  -c srs'
alias vglo='su - ${sid}adm  -c vglo'