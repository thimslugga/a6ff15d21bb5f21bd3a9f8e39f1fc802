# Pacemaker Cluster Resource Manager (CRM) Cheatsheet

Pacemaker Cluster Resource Manager (CRM)

- https://clusterlabs.org/pacemaker/doc/2.1/Pacemaker_Administration/html/pcs-crmsh.html
- https://people.redhat.com/kgaillot/pacemaker/doc/2.1/Pacemaker_Administration/pdf/Pacemaker_Administration.pdf

## Installation

RHEL:

```sh
dnf install -y firewalld pcs pacemaker corosync resource-agents fence-agents-all 
```

```sh
dnf install -y resource-agents-cloud
```

SLES:

## PCS

- https://access.redhat.com/articles/3681151

### Cluster Setup with pcs

```sh
passwd hacluster
```

```shell
firewall-cmd --state
firewall-cmd --permanent --add-service=high-availability
firewall-cmd --add-service=high-availability
```

```shell
systemctl enable --now pcsd.service
```

```shell
pcs cluster auth <host01> <host02> <host03>
```

```shell
pcs cluster setup
```

### Cluster Status

```shell
pcs status
pcs status --full
pcs status --debug
```

```shell
crm_mon -1
```

```sh
crm_mon --one-shot --inactive
strace -Tttvfs 1024 -o /tmp/strace.out /usr/sbin/crm_mon --one-shot --inactive
```

```sh
pcs status --full 
```

### Cluster Manager Configuration

```sh
pcs config
pcs cluster cib
```

Upgrade CIB:

```
pcs cluster cib-upgrade
```

### Cluster Properties

```sh
pcs property list --all
pcs property list --defaults

pcs property show --defaults
pcs property show cluster-infrastructure

pcs property show property
```

### Resource Agents

```sh
pcs resource standards
```

```sh
pcs resource agents ocf
pcs resource agents lsb
pcs resource agents service
pcs resource agents stonith
pcs resource agents
```

```sh
pcs resource agents ocf:pacemaker
```

```sh
pcs resource describe <resource_agent>

# example
pcs resource describe IPaddr2
pcs resource describe ocf:heartbeat:IPaddr2
```

```sh
pcs resource show
pcs resource show --full
pcs resource show <resource_name>
```

```sh
pcs resource config
```

### Fencing aka Stonith Resource

- https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_high_availability_clusters/assembly_configuring-fencing-configuring-and-managing-high-availability-clusters#proc_configuring-acpi-for-fence-devices-configuring-fencing

```sh
pcs stonith show
pcs stonith show --full
```

List stonith resources:

```sh
pcs stonith list
ls -lA /usr/sbin/fence*
```

```sh
pcs stonith describe <stonith agent>
pcs stonith describe <stonith agent> --full
```

Show the options for fence_aws stonith script:

```sh
pcs stonith describe fence_aws --full
```

Update hosts and instance IDs for existing stonith resource named `clusterfence`:

```sh
pcs stonith update clusterfence fence_aws pcmk_host_map="<hostname>:<instance_id>;<hostname>:<instance_id>"
```

```sh
pcs property set startup-fencing=true
pcs property set concurrent-fencing=true
pcs property set stonith-action=reboot
pcs property set stonith-action=reboot
pcs property set stonith-timeout=300s
pcs property set stonith-max-attempts=10
```

```sh
pcs property set stonith-enabled=true
pcs property set stonith-enabled=false
```

```sh
pcs property set maintenance-mode=false
```

```sh
pcs property set no-quorum-policy=ignore
pcs property set symmetic-cluster=

# Create an opt-in cluster, which prevents resources from running anywhere by default
pcs property set symmetric-cluster=false

# Create an opt-out cluster, which allows resources to run everywhere by default
pcs property set symmetric-cluster=true

pcs property set shutdown-escalation=20min
```

```sh
#pcs stonith create name fencing_agent parameters
pcs stonith create rhev-fence fence_rhevm ipaddr=engine.local.net ipport=443 ssl_insecure=1 ssl=1 inet4_only=1 login=admin@internal passwd=PASSWD pcmk_host_map="clu01:clu01.local.net;clu02:clu02.local.net;clu03:clu03.local.net" pcmk_host_check=static-list pcmk_host_list="clu01.local.net,clu02.local.net,clu03.local.net" power_wait=3 op monitor interval=90s
pcs stonith update rhev-fence fence_rhevm api_path=/ovirt-engine/api disable_http_filter=1 ipaddr=engine.local.net ipport=443 ssl_insecure=1 ssl=1 inet4_only=1 login=admin@internal passwd=PASSWORD pcmk_host_map="clu01.local.net:clu01.local.net;clu02.local.net:clu02.local.net;clu03.local.net:clu03.local.net" pcmk_host_check=static-list pcmk_host_list="clu01.local.net,clu02.local.net,clu03.local.net" power_wait=3 op monitor interval=90s
pcs stonith update rhev-fence port=nodeb
pcs stonith show rhev-fence
```

```sh
fence_rhevm -o status -a engine.local.net --username=admin@internal --password=PASSWORD --ipport=443  -n clu03.local.net -z --ssl-insecure  --disable-http-filter 
pcs stonith fence hostname
```

Note: When Pacemaker's policy engine creates a transition with a fencing request, the stonith daemon uses the timeout value that is passed by the transition engine. This matches the value of the `stonith-timeout` cluster property.

When fencing is triggered manually via `stonith_admin` or `pcs stonith fence`, the default timeout implemented in stonith_admin (120s) is used instead.

```sh
pcs stonith delete fence_noded
```

#### Disable SBD

```shell
systemctl disable --now sbd
pcs property set stonith-watchdog-timeout=0
pcs cluster stop --all
pcs cluster start --all
pcs property unset stonith-watchdog-timeout
```

### Cluster Notifications

```
pcs resource create webmail MailTo email=user@domain.com subject="CLUSTER-NOTIFICATINS" --group=firstweb
vim /usr/local/bin/crm_notify.sh
pcs resource create mailme ClusterMon extra_options="-e mail@domain.com -E /usr/local/bin/crm_notify.sh --clone
```


## CRM Shell aka crmsh

- https://crmsh.github.io/
- https://crmsh.github.io/documentation/

crmsh configuration file:

```
/etc/crm/crm.conf
~/.config/crm/crm.conf
~/.crm.rc
```

### Use Cluster ACLs

```sh
crm options user hacluster
```

```sh
cat /etc/sudoers
ls -lA /etc/sudoers.d/
```

```sh
crm configure property enable-acl=true
```

Now all users for whom you want to modify access rights with ACLs must belong to the `haclient` group. 

### Cluster Status

View cluster status using crmsh:

```sh
crm status
```

```sh
watch -n 1 -c crm status
```

```sh
crm_mon -1
crm_mon -1Ar
```

Troubleshooting comamnds:

```sh
crm cluster health
```

```sh
crm_mon --one-shot --inactive
strace -Tttvfs 1024 -o /tmp/strace.out /usr/sbin/crm_mon --one-shot --inactive
```

### Cluster Configuration

```sh
/usr/sbin/crm configure show

/usr/sbin/crm configure show | grep cli-

/usr/sbin/crm configure show obscure:passw* | grep cli-
```

```sh
cibadmin -Q
```

Show raw configuration:

```
/usr/sbin/crm configure show xml
```

Show options

```
crm configure show cib-bootstrap-options
crm configure show rsc-options
crm configure show op-options
```

```sh
crm configure show SAPHanaSR
```

### Upgrade CIB Syntax Version

Sometimes new features are only available with the latest CIB syntax version. When you upgrade to a new product version, your CIB syntax version will not be upgraded by default.

Check your version with:

```
cibadmin -Q | grep validate-with
```

Upgrade to the latest CIB syntax version with:

```
cibadmin --upgrade --force
```

### Resource Agents

```sh
crm ra classes

crm ra list ocf pacemaker
```

### Fencing aka Stonith Resource

```sh
crm configure property stonith-enabled=true
crm configure property stonith-enabled=false
```

## SAPHanaSR

```sh
SAPHanaSR-showAttr
SAPHanaSR-monitor
```

## ClusterTools2

```shell
cs_clusterstate -i
cs_show_error_patterns -c | grep -v "=.0"
cs_show_memory
cs_sum_base_config
```

SAP HANA:

```shell
cs_show_hana_autofailover --all
cs_show_hana_info --info $SID $nr
```

## SAP

### SAP Shared File Systems

```
/hana/shared
```

For the SAP instances to work, the following shared file systems must be available on all cluster nodes:

```
/sapmnt
/usr/sap/trans
```

The shared file systems can either be managed by the cluster or they can be statically mounted by adding them to the /etc/fstab on each cluster node. 

### SAP Startup Sevice Framework

```
ps aux | grep <SID>adm | grep sapstartsrv
```

### sapcontrol

```
sapcontrol -nr $nr -function GetSystemInstanceList
sapcontrol -nr $nr -function HAGetFailoverConfig
sapcontrol -nr $nr -function HACheckFailoverConfig
```

```
#sapcontrol -nr $nr -function StopService
#sapcontrol -nr $nr -function StartService <SID> 
#sapcontrol -nr $nr -function StartSystem
#sapcontrol -nr $nr -function StopSystem ALL
```

### SAP HANA System Replication (SR)

```
HDBsettings.sh systemOverview.py
HDBSettings.sh systemReplicationStatus.py; echo RC:$?
HDBSettings.sh landscapeHostConfiguration.py; echo RC:$?
```

```
hdbnsutil -sr_state
```